package JTest;

import Code.CalculatorView;
import junit.framework.Assert;
import junit.framework.TestCase;

public class viewTest extends TestCase {
	private CalculatorView objCalcUnderTest;
    
    public void setUp() {
        objCalcUnderTest = new CalculatorView();
    }
    
    public void testAdd() {
        int a = 15;
        int b = 20;
        int expectedResult = 35;
        int result = objCalcUnderTest.add(a, b);
        Assert.assertEquals(expectedResult, result);;
    }
    
    public void testSubtract() {
        int a = 25;
        int b = 20;
        int expectedResult = 5;
        long result = objCalcUnderTest.sub(a, b);
        Assert.assertEquals(expectedResult, result);;
    }
    
    public void testMultiply() {
        int a = 10;
        int b = 25;
        long expectedResult = 250;
        long result = objCalcUnderTest.mul(a, b);
        Assert.assertEquals(expectedResult, result);;
    }
    
    public void testDivide() {
        int a = 56;
        int b = 10;
        double expectedResult = 5;
        double result = objCalcUnderTest.div(a, b);
        Assert.assertEquals(expectedResult, result);
    }
   
}
