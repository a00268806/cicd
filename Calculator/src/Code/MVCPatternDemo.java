package Code;

import java.util.Scanner;

public class MVCPatternDemo {
	   public static void main(String[] args) {

	      //fetch student record based on his roll no from the database
	      Calculator model  = addValue();

	      //Create a view : to write student details on console
	      CalculatorView view = new CalculatorView();

	      CalculatorController controller = new CalculatorController(model, view);

	      controller.updateView();
	      
	     
	      

	   }

	   private static Calculator addValue(){
	      Calculator student = new Calculator();
	      Scanner input = new Scanner(System.in);
	      System.out.println("Enter Number 1 =");
	      int number1 = input.nextInt();
	      System.out.println("Enter Number 2 =");
	      int number2 = input.nextInt();
	      student.setNum1(number1);
	      student.setNum2(number2);
	      return student;
	   }
	   
	}