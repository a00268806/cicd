package Code;

public class CalculatorController {
	   private Calculator model;
	   private CalculatorView view;

	   public CalculatorController(Calculator model, CalculatorView view){
	      this.model = model;
	      this.view = view;
	   }

	   public void setnumber1(int n1){
	      model.setNum1(n1);		
	   }

	   public int getnumber1(){
	      return model.getNum1();		
	   }

	   public void setnumber2(int n2){
	      model.setNum2(n2);		
	   }

	   public void updateView(){				
	      view.printStudentDetails(model.getNum1(), model.getNum2());
	      view.add(model.getNum1(), model.getNum2());
	      view.div(model.getNum1(), model.getNum2());
	      view.sub(model.getNum1(), model.getNum2());
	      view.mul(model.getNum1(), model.getNum2());
	   }	
	}
