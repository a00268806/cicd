rm -rf Calculator/bin
mkdir Calculator/bin
find Calculator/src -type f -name '*.java' -exec javac -d Calculator/bin -cp Calculator/lib/junit.jar {} +
if [ $? -ne 0 ]; then
	exit 1;
fi

cd Calculator/bin
find ./ -type f -name '*.class' -exec jar cfe NewspaperDeliverySystem.jar Create_Cust {} +
if [ $? -ne 0 ]; then
  echo "Jar Not creating";
else
  echo "Jar creating";
fi 

find ./ -type f -printf "%f\n"